package com.yln.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring boot的main()
 * */
@SpringBootApplication()
public class DesignpatternApplication {
    public static void main(String[] args) {
        SpringApplication.run(DesignpatternApplication.class, args);
    }

}
