package com.yln.demo.lambda;

public class WhatIsThis {

    public Runnable r1(){
        return () -> System.out.println(this.toString());
    }

    public Runnable r2(){
        return new Runnable() {
            public void run() {
                System.out.println(this.toString());
            }
        };
    }

    @Override
    public String toString(){
        return "I am 'What is this' Object.";
    }
}
