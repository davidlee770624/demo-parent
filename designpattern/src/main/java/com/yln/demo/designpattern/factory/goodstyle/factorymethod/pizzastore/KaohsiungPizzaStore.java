package com.yln.demo.designpattern.factory.goodstyle.factorymethod.pizzastore;

import com.yln.demo.designpattern.factory.pizza.GreekPizza;
import com.yln.demo.designpattern.factory.pizza.PepperoniPizza;
import com.yln.demo.designpattern.factory.pizza.Pizza;

public class KaohsiungPizzaStore extends PizzaStore {

    // 高雄店才有賣pepperoni pizza
    @Override
    Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("special")){
            pizza = new PepperoniPizza();
        }else if(type.equals("greek")){
            pizza = new GreekPizza();
        }
        return pizza;
    }
}
