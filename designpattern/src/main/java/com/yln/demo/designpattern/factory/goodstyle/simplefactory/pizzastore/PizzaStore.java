package com.yln.demo.designpattern.factory.goodstyle.simplefactory.pizzastore;

import com.yln.demo.designpattern.factory.goodstyle.simplefactory.factory.SimplePizzaFactory;
import com.yln.demo.designpattern.factory.pizza.Pizza;

public class PizzaStore {

    SimplePizzaFactory factory;

    public PizzaStore(SimplePizzaFactory factory) {
        this.factory = factory;
    }

    // client
    public Pizza orderPizza(String type) {
        // pizza的創造
        Pizza pizza = factory.createPizza(type);
        // pizza的使用
        pizza.prepare();
        pizza.cook();
        return pizza;
    }

}
