package com.yln.demo.designpattern.factory.goodstyle.simplefactory.factory;

import com.yln.demo.designpattern.factory.pizza.CheesePizza;
import com.yln.demo.designpattern.factory.pizza.GreekPizza;
import com.yln.demo.designpattern.factory.pizza.PepperoniPizza;
import com.yln.demo.designpattern.factory.pizza.Pizza;

// 簡單工廠模式
public class SimplePizzaFactory {
    // 跟bad style做比較 , 僅是把PizzaStore 的if-else邏輯統一集中在工廠裡
    // 優點就是分離了物件的使用和創造, 隱藏了創建物件的細節，加入新產品不需要改動client

    public Pizza createPizza(String type){
        Pizza pizza = null;
        if(type.equals("cheese")){
            pizza = new CheesePizza();
        }else if(type.equals("greek")){
            pizza = new GreekPizza();
        }else if(type.equals("pepperoni")){
            pizza = new PepperoniPizza();
        }
        return pizza;
    }
}
