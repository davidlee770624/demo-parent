package com.yln.demo.designpattern.strategy;

import org.springframework.stereotype.Component;

@Component
public class ClangulaDuck implements Duck {
    @Override
    public void fly() {
        System.out.println("Clangula Duck 隨風飛行");
    }
}
