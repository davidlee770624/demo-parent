package com.yln.demo.designpattern.singleton;

public class Father {
    // singleton代表最多只有一個instance 代表說我不能讓任何人想建instance就建instance

    // volatile=> 當第一個thread可能還在new實例 , 第二個thread就把尚未初始化完成的物件return
    // 我的讀會保證其他的thread寫完之後
    private volatile static Father father;//Initialized to null

    // 唯一解法就是需要private constructor 這個constructor只有這個class本身的function才可以call
    private Father(){

    }

    // 取得唯一instance
    public static Father getInstance(){
        // 避免重複new實例
        if(father == null){
            // 併發考量
            synchronized(Father.class){
                // Double checked locking
                if(father == null) {
                    father = new Father();
                }
            }
        }
        return father;
    }

}
