package com.yln.demo.designpattern.factory.pizza;

import com.yln.demo.designpattern.factory.goodstyle.abstractfactory.factory.IngredientFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.PastryFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.SauceFactory;

public class CheesePizza implements Pizza {

    // 簡單工廠方法
    public CheesePizza(){

    }

    // 套用工廠方法
    public CheesePizza(SauceFactory sauceFactory, PastryFactory pastryFactory){

    }

    // 套用抽象工廠方法
    public CheesePizza(IngredientFactory ingredientFactory){

    }

    @Override
    public void prepare() {
        System.out.println("準備Cheese Pizza材料");
    }

    @Override
    public void cook() {
        System.out.println("亨煮Cheese Pizza");
    }
}
