package com.yln.demo.designpattern.factory.pizza;

public class PepperoniPizza implements Pizza {
    @Override
    public void prepare() {
        System.out.println("準備Pepperoni Pizza材料");
    }

    @Override
    public void cook() {
        System.out.println("亨煮Pepperoni Pizza");
    }
}
