package com.yln.demo.designpattern.factory.goodstyle.abstractfactory.pizzastore;

import com.yln.demo.designpattern.factory.pizza.Pizza;

// 抽象工廠方法模式
public abstract class PizzaStore {
    // 與工廠方法差別在於 食材的部分抽離成介面
    // 用一個抽象工廠來定義一個創建 產品族 的介面，產品族裡面每個產品的具體類別由繼承抽象工廠的實體工廠決定
    // 產品族好比如 房子內的家具、pizza的食材
    // 抽象工廠定義了需要創建的產品族，由 concreteFactory 去實作抽象工廠，所以通常抽象工廠裡的每一個創建 product 的抽象方法，都是用工廠方法，由繼承的具體工廠來實現，這也是這兩個名詞常被搞混的原因。
    abstract Pizza createPizza(String type);

    // client
    public Pizza orderPizza(String type) {
        // 父類別還可以 call子類別實作的函數。
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.cook();
        return pizza;
    }
}
