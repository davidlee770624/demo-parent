package com.yln.demo.designpattern.factory.badstyle;

import com.yln.demo.designpattern.factory.pizza.CheesePizza;
import com.yln.demo.designpattern.factory.pizza.GreekPizza;
import com.yln.demo.designpattern.factory.pizza.PepperoniPizza;
import com.yln.demo.designpattern.factory.pizza.Pizza;

public class PizzaStore {

    // 只要有新的 Pizza 推出或是有舊的要拿掉，需要改這裡的 if-else 有點麻煩
    public Pizza createPizza(String type){
        Pizza pizza = null;
        if(type.equals("cheese")){
            pizza = new CheesePizza();
        }else if(type.equals("greek")){
            pizza = new GreekPizza();
        }else if(type.equals("pepperoni")){
            pizza = new PepperoniPizza();
        }
        pizza.prepare();
        pizza.cook();
        return pizza;
    }
}
