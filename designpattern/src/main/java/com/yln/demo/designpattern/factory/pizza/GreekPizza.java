package com.yln.demo.designpattern.factory.pizza;

public class GreekPizza implements Pizza{

    @Override
    public void prepare() {
        System.out.println("準備Greek Pizza材料");
    }

    @Override
    public void cook() {
        System.out.println("亨煮Greek Pizza");
    }
}
