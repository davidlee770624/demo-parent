package com.yln.demo.designpattern.strategy;

import org.springframework.stereotype.Component;

@Component
public class SteamerDuck implements Duck {
    @Override
    public void fly() {
        System.out.println("Steamer Duck 滑翔");
    }
}
