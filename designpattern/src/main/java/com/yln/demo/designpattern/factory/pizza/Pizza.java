package com.yln.demo.designpattern.factory.pizza;

public interface Pizza {
    String type = "";
    void prepare();
    void cook();
}
