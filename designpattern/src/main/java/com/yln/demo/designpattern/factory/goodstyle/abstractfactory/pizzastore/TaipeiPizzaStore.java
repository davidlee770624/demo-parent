package com.yln.demo.designpattern.factory.goodstyle.abstractfactory.pizzastore;

import com.yln.demo.designpattern.factory.goodstyle.abstractfactory.factory.ClassicIngredientFactory;
import com.yln.demo.designpattern.factory.goodstyle.abstractfactory.factory.IngredientFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.MexicoPastryFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.NewYorkSauceFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.PastryFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.SauceFactory;
import com.yln.demo.designpattern.factory.pizza.CheesePizza;
import com.yln.demo.designpattern.factory.pizza.GreekPizza;
import com.yln.demo.designpattern.factory.pizza.Pizza;

public class TaipeiPizzaStore extends PizzaStore {

    // 台北店才有賣cheese pizza , greek pizza
    @Override
    Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("special")){
            // 食材的部分 , 僅一個參數
            IngredientFactory ingredientFactory = new ClassicIngredientFactory();
            pizza = new CheesePizza(ingredientFactory);
        }else if(type.equals("greek")){
            pizza = new GreekPizza();
        }
        return pizza;
    }
}
