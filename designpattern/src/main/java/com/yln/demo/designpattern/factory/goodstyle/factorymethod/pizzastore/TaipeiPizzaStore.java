package com.yln.demo.designpattern.factory.goodstyle.factorymethod.pizzastore;

import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.MexicoPastryFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.NewYorkSauceFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.PastryFactory;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.factory.SauceFactory;
import com.yln.demo.designpattern.factory.pizza.CheesePizza;
import com.yln.demo.designpattern.factory.pizza.GreekPizza;
import com.yln.demo.designpattern.factory.pizza.Pizza;

public class TaipeiPizzaStore extends PizzaStore {

    // 台北店才有賣cheese pizza , greek pizza
    @Override
    Pizza createPizza(String type) {
        Pizza pizza = null;
        if(type.equals("special")){
            // 缺點 : pizza創造 , 當食材細分2種以上 , 並且每項食材都有一個工廠 , 故食材有了抽象工廠模式
            SauceFactory sauceFactory = new NewYorkSauceFactory();
            PastryFactory pastryFactory = new MexicoPastryFactory();
            pizza = new CheesePizza(sauceFactory,pastryFactory);
        }else if(type.equals("greek")){
            pizza = new GreekPizza();
        }
        return pizza;
    }
}
