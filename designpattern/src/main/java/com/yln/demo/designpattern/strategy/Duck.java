package com.yln.demo.designpattern.strategy;

public interface Duck {
    void fly();
}
