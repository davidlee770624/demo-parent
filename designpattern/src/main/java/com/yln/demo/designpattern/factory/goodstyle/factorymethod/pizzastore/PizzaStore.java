package com.yln.demo.designpattern.factory.goodstyle.factorymethod.pizzastore;

import com.yln.demo.designpattern.factory.pizza.Pizza;

// 工廠方法模式
public abstract class PizzaStore {
    // 定義了一個建立物件的介面，但由子類決定要實例化的類別為何。工廠方法讓類別把 實例化 的動作推遲到了子類。
    // 原本物件的建立，交給一個外來的工廠(Simple Factory)處理，現在我把它交給我的子類別處理，而且父類別還可以 call子類別實作的函數。
    // 優點 : 分離了物件的使用和創造, 隱藏了創建物件的細節，加入新產品不需要改動client
    // 缺點 : pizza創造 , 食材難以細分且每項食材都有一個工廠 , 故食材有了抽象工廠模式
    abstract Pizza createPizza(String type);

    // client
    public Pizza orderPizza(String type) {
        // 父類別還可以 call子類別實作的函數。
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.cook();
        return pizza;
    }
}
