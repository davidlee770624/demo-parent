package org.slf4j.impl;

import org.slf4j.impl.LoggerFactory;
import com.yln.demo.designpattern.singleton.Father;
import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

// 包名一定要注意，根据slf4j的查找规则，必须这样子定义
// 1. StaticLoggerBinder，这就是用来获取日志工厂的
public class StaticLoggerBinder implements LoggerFactoryBinder {

    private static StaticLoggerBinder SINGLETON = new StaticLoggerBinder();
    private static final String loggerFactoryClassStr = LoggerFactory.class.getName();
    private volatile static ILoggerFactory loggerFactory;

    private StaticLoggerBinder() {
    }

    @Override
    public ILoggerFactory getLoggerFactory() {
        if(loggerFactory == null){
            synchronized(Father.class){
                if(loggerFactory == null) {
                    loggerFactory = new LoggerFactory();
                }
            }
        }
        return loggerFactory;
    }

    @Override
    public String getLoggerFactoryClassStr() {
        return LoggerFactory.class.getName();
    }

    public static StaticLoggerBinder getSingleton() {
        return SINGLETON;
    }
}
