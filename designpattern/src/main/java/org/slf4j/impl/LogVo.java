package org.slf4j.impl;

import lombok.Data;

@Data
public class LogVo {
    private String level;
    private String content;
    private String className;
    private String timestamp;

}
