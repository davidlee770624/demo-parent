package org.slf4j.impl;

import org.slf4j.impl.Logger;
import org.slf4j.ILoggerFactory;

// 包名一定要注意，根据slf4j的查找规则，必须这样子定义
// 2. 实现自己的LoggerFactory
public class LoggerFactory implements ILoggerFactory {
    // 執行續安全
    ThreadLocal<Logger> loggers = new ThreadLocal<>();

    @Override
    public Logger getLogger(String name) {
        Logger logger = loggers.get();
        if (logger != null) {
            return logger;
        } else {
            Logger newInstance = new Logger(name);
            loggers.set(newInstance);
            return newInstance;
        }
    }
}
