package org.slf4j.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// 包名一定要注意，根据slf4j的查找规则，必须这样子定义
public class LoggerUtil {
    // https://zhuanlan.zhihu.com/p/118447447

    /**
     * Java8日期格式化，线程安全
     */
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 日志发送接口
     */
    public static final String URI = "保密";

    public void info(String msg) {
        write(new Exception().getStackTrace()[1].getClassName() + " : " + new Exception().getStackTrace()[1].getMethodName(), "info", msg);
    }

    public void warn(String msg) {
        write(new Exception().getStackTrace()[1].getClassName() + " : " + new Exception().getStackTrace()[1].getMethodName(),"warn", msg);
    }

    public void error(String msg) {
        write(new Exception().getStackTrace()[1].getClassName() + " : " + new Exception().getStackTrace()[1].getMethodName(),"error", msg);
    }

    public void debug(String msg) {
        write(new Exception().getStackTrace()[1].getClassName() + " : " + new Exception().getStackTrace()[1].getMethodName(),"debug", msg);
    }

    public void write(String className, String level, String content) {
        LogVo logVo = new LogVo();
        logVo.setLevel(level);
        logVo.setContent(content);
        logVo.setClassName(className);
        logVo.setTimestamp(LocalDateTime.now().format(dtf));
        System.out.println("寫入LOG：" + logVo);
    }
}
