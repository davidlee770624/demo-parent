package com.yln.demo.lambda;

import com.yln.demo.designpattern.singleton.Father;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestLambda {
    // https://magiclen.org/java-8-lambda/

    // 何謂Lambda , 針對「只擁有一個方法的介面」所實作出來的匿名類別,但本質跟匿名類別不一樣
    // 在Java 8之前，我們將這類的介面稱為Single Abstract Method type(SAM)；
    // 在Java 8之後，因為這類的介面變得十分重要，所以將其另稱為Functional Interface。
    // 語法結構 , input -> body
    // 優點 , 簡易+效能好 , 因為它如同static存放記憶體空間 , 避免掉產生新的.class檔案出來
    // 注意 , Lambda運算式的外部變數，一旦宣告並存取之後 ，就不能再去修改(參照位址) , 是為了要保證多執行緒安全

    @Test
    public void testLambda() throws InterruptedException {

        // 在Java 8之前
        Runnable runnbale1 = new Runnable() {
            public void run() {
                System.out.println("run me!");
            }
        };

        // 在Java 8之後
        Runnable runnbale2 = () -> System.out.println("run me!");

        // 因為它如同static存放記憶體空間 , 不會額外new實例
        for(int i=1;i<=100;i++) {
            Runnable runnbale3 = () -> System.out.println("run me!");
            System.out.println(runnbale3);
        }

    }

    // https://affairs.kh.edu.tw/study/2016-java2/04.html
    // 當在Lambda運算式中使用this關鍵字時，指的是「建立Lambda方法的this」，
    // 而不是「Lambda方法中的this」，從此點即可得知Lambda並不等同於匿名類別
    @Test
    public void testLambda2() throws InterruptedException {

        new Thread(new WhatIsThis().r1()).start();
        new Thread(new WhatIsThis().r2()).start();

    }

    // https://magiclen.org/java-8-lambda/
    // 走訪、過濾和一些簡單的運算 Collection
    @Test
    public void testLambda3() throws InterruptedException {

        // 在Java 8之前
        List<String> list = new ArrayList<>();
        for (String s : list) {
            System.out.print(s);
        }
        Map<String,String> map = new HashMap();
        Set<String> keySet = map.keySet();
        for (String s : keySet) {
            System.out.print(s + ":" + map.get(s));
        }

        // 在Java 8之後
        list.forEach(s -> System.out.print(s));
        map.forEach((k, v) -> System.out.print(k + ":" + v));

    }

    // 過濾和基本運算
    // stream() & parallelStream()
    @Test
    public void testLambda4() throws InterruptedException {

        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("5");
        list.add("4");
        list.stream().filter(s -> Integer.valueOf(s) < 3).forEach(s -> System.out.print(s));

        // 換行
        System.out.println("");

        List<String> list2 = list.stream().filter(s -> Integer.valueOf(s) < 3).collect(Collectors.toList());
        list2.add("7");
        list2.forEach(s -> System.out.print(s));

        // 換行
        System.out.println("");

        List<String> list3 = new ArrayList<String>();
        list3.add("1");
        list3.add("2");
        list3.add("3");
        list3.add("4");
        list3.add("5");
        System.out.println(list3.stream().mapToInt(s->Integer.valueOf(s)).sum());
        System.out.println(list3.stream().filter(s -> Integer.valueOf(s) < 3).mapToInt(s->Integer.valueOf(s)).average().getAsDouble());
    }

    // https://magiclen.org/java-8-lambda/
    // Lambda特殊精簡語法結構 , 方法名稱
    @Test
    public void testLambda5() throws InterruptedException {

        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.forEach(System.out::print);

    }

}

