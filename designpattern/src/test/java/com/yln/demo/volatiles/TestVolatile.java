package com.yln.demo.volatiles;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestVolatile {
    // volatile https://blog.csdn.net/yjp198713/article/details/78839698
    // join https://www.cnblogs.com/duanxz/p/5038471.html
    // sleep、yield、wait、join  https://www.cnblogs.com/aspirant/p/8876670.html

    //volatile // 有序性 可見性
    private volatile static int x = 0, y = 0;
    private volatile static int a = 0, b = 0;

    @Test
    public void testVolatile() throws InterruptedException {
        // 執行程式時為了提高效率，因此compiler和processor可以對指令做重新排序的動作
        // JVM可能會為了優化代碼,而改變變數的執行順序 , 導致多執行續不安全
        // (1,0)  two完全執行完->one完全執行完
        // (0,1)  one完全執行完->two完全執行完
        // (1,1)  one與two交錯執行


        // (0,0)  one與two交錯執行,但發生指令重排序
        int i=0;
       while (true){
           i++;
           x = 0;
           y = 0;
           a = 0;
           b = 0;
           Thread one = new Thread(new Runnable() {
               public void run() {
                   a = 1;
                   x = b;
               }
           });

           Thread two = new Thread(new Runnable() {
               public void run() {
                   b = 1;
                   y = a;
               }
           });

           one.start();
           two.start();
           one.join();
           two.join();
           System.out.println(i);
           if (x == 0 && y == 0) {
               String result = "第" + i + "次循环" + "x =" + x + " y=" + y;
               System.out.println(result);
               break;
           }
       }

    }
}

