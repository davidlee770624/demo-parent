package com.yln.demo.designpattern.facade;

import com.yln.demo.DesignpatternApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DesignpatternApplication.class)
@Slf4j
public class TestFacade {
	// https://zhuanlan.zhihu.com/p/118447447
    @Test
    public void testLogger() {
        log.info("HAHAHAHAHA");
    }
}
