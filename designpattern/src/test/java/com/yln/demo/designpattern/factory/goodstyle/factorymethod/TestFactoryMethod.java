package com.yln.demo.designpattern.factory.goodstyle.factorymethod;

import com.yln.demo.designpattern.factory.goodstyle.factorymethod.pizzastore.PizzaStore;
import com.yln.demo.designpattern.factory.goodstyle.factorymethod.pizzastore.TaipeiPizzaStore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestFactoryMethod {
    // https://blog.techbridge.cc/2017/05/22/factory-method-and-abstract-factory/
    @Test
    public void orderPizza() {
        // 點取招牌pizzy , 每家店都不同
        PizzaStore pizzaStore = new TaipeiPizzaStore();
        pizzaStore.orderPizza("special");
    }
}
