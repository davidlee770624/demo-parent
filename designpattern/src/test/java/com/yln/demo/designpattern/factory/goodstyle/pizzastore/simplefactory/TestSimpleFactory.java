package com.yln.demo.designpattern.factory.goodstyle.pizzastore.simplefactory;

import com.yln.demo.designpattern.factory.goodstyle.simplefactory.pizzastore.PizzaStore;
import com.yln.demo.designpattern.factory.goodstyle.simplefactory.factory.SimplePizzaFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSimpleFactory {
    // https://blog.techbridge.cc/2017/05/22/factory-method-and-abstract-factory/
    @Test
    public void orderPizza() {
        // 點餐 cheese pizza
        PizzaStore pizzaStore = new PizzaStore(new SimplePizzaFactory());
        pizzaStore.orderPizza("cheese");
    }
}
