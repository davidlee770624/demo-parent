package com.yln.demo.designpattern.strategy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestStrategy {
    // https://www.jyt0532.com/2017/04/07/strategy/

    @Autowired
    private List<Duck> ducks;

    @Test
    public void test() {
        for(Duck duck:ducks)
            duck.fly();

    }
}
