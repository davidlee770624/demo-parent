package com.yln.demo.designpattern.singleton;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSingleton {
    // https://www.jyt0532.com/2017/05/19/singleton/
    @Test
    public void getFather() {
        for(int i=1;i<=100;i++) {
            new Thread(()-> System.out.println( Father.getInstance()) ).start();
        }
    }
}
