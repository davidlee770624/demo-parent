package com.yln.demo.aop;

import com.yln.demo.aop.service.DemoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestAOP {

    @Autowired
    DemoService demoService;

    @Test
    public void testDelete() {
        try {
            demoService.delete(1);
        }catch (Exception e){}
    }

    /**
     * 底層實現位置 =>  jdk動態代理 or CGLIB動態代理
     */
//@around-start
//  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        Object result;
             //@Before
//        try {
//
//                result = method.invoke(target, args);
//        } catch (InvocationTargetException e) {
//            Throwable targetException = e.getTargetException();
//            //@AfterThrowing
//            throw targetException;
//        } finally {
//            //@AfterReturning
//        }
//                return result;
             //@After
//  }
//@around-end

}
