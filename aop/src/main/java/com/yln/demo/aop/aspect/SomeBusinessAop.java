package com.yln.demo.aop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
*  來源 https://juejin.im/post/6844903766035005453
*
 * @Aspect SomeBusinessAop宣告為切面 , 單個method被多個aspect同時切面要加入順序@Order(1)
 * @Component SomeBusinessAop納入 spring container管理 = bean
 *
* */
@Aspect
@Component
public class SomeBusinessAop {

    /**
     * @Pointcut 宣告切面的位置
     *
     * */
    @Pointcut("execution(* com.yln.demo.aop.service.imp.DemoServiceImp.*(..))")
//    @annotation
    public void pointcut() {
    }

    @Before("pointcut()")
    public void before() {
        System.out.println("@Before: begin Transaction");
    }

    @After("pointcut()")
    public void after() {
        System.out.println("@After: write log");
    }

    @AfterReturning( pointcut = "pointcut()", returning = "returnObject")
    public void afterReturning(JoinPoint joinPoint, Object returnObject) {
        System.err.println("@AfterReturning: Transaction commit");
    }

    @AfterThrowing("pointcut()")
    public void afterThrowing() {
        System.err.println("@AfterThrowing: Transaction rollback");
    }

    @Around("pointcut()")
    public void around(ProceedingJoinPoint joinPoint){
        try {
            System.out.println("@Around: before log");
            joinPoint.proceed(); // 執行'被代理'對象的原程序-> DemoService.delete()
        } catch (Throwable e) {
//            e.printStackTrace();
//            throw e;
        } finally {
            System.out.println("@Around: after log");
        }
    }

}
