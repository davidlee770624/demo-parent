package com.yln.demo.aop.service.imp;


import com.yln.demo.aop.service.DemoService;
import org.springframework.stereotype.Component;

/**
 * spring 採用代理模式 生成bean (aop概念), 所以必須繼承介面
 * spring判斷到 implement interface => jdk代理
 * spring判斷到 extend class => cglib代理
 *
 * */
@Component
public class DemoServiceImp implements DemoService {

    @Override
    public void delete(int id) throws Exception {
        System.out.println("刪除資料 id=" +id );
//        throw new Exception();
    }
}
